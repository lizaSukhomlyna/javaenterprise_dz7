<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<center>
    <h1>Hello ${pageContext.request.userPrincipal.name} !</h1>
    <sec:authorize access="!isAuthenticated()">
        <h4><a href="/login">Log in</a></h4>
        <h4><a href="/registration">Registration</a></h4>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <h4><a href="/logout">Log out</a></h4>
    </sec:authorize>
    <h4><a href="/admin">Users (only for admin)</a></h4>
    <h4><a href="/shop/index">Shop(only for users)</a></h4>

</center>
</body>
</html>