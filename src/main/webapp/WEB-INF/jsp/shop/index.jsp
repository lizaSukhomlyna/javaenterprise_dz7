<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Spring Boot Form Handling Example</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">

</head>
<body>
<h1>Hello ${pageContext.request.userPrincipal.name} !</h1>
<p>
    <a href="../index">Return to main page</a>
    <a href="/card/cardAdd">Add card</a>
    <a href="/card/allCardsShow">Show all cards</a>
    <a href="/product/productAdd">Add product</a>
    <a href="/product/allProductShow">Show all product</a>
</p>


</body>
</html>