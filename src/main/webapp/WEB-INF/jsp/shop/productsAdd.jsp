<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration Form</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
<body>
    <center>
        <h2>Adding products</h2>

        <form:form action="productAdd" method="post" modelAttribute="product">


            <form:label path="id">Product id:</form:label>
            <form:input type="number" path="id"/><br/>

            <form:label path="card_id">Card id:</form:label>
             <form:select path="card_id" items="${cardIdList}" /><br/>

            <form:label path="name">Name:</form:label>
            <form:input path="name"/><br/>

            <form:label path="cost">Cost:</form:label>
            <form:input type="number" path="cost"/><br/>
            <form:button>Register</form:button>
        </form:form>
    </center>
    <h2>Return to pages</h2>
    <p>
        <a href="../index">Return to main page</a>
        <a href="/card/cardAdd">Add card</a>
        <a href="/card/allCardsShow">Show all cards</a>
        <a href="/product/productAdd">Add product</a>
        <a href="/product/allProductShow">Show all product</a>
    </p>
</body>
</html>