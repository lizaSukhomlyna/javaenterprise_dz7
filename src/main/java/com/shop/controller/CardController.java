package com.shop.controller;

import com.shop.entity.User;
import com.shop.repository.UserRepository;
import com.shop.service.CardService;
import com.shop.dtos.CardDto;
import com.shop.entity.Card;
import com.shop.repository.CardRepository;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping(path = "/card")
public class CardController {

    @Autowired
    private CardService cardService;

    @Autowired
    private UserService userService;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    UserRepository personRepository;

    @PostMapping("/cardAdd")
    public String submitFormCardAdd(@ModelAttribute("card") CardDto cardDto) {
        Card card = new Card();
        card.setIdCard(cardDto.getIdCard());
        card.setIdPerson(cardDto.getIdPerson());
        card.setCost(BigDecimal.ZERO);
        card.setProducts(cardDto.getProducts());
        cardService.createCardByPersonId(card);
        return "shop/cardAddSucces";
    }

    @RequestMapping(value = "/cardAdd", method = RequestMethod.GET)
    public String showFormCardAdd(Model model) {
        CardDto card = new CardDto();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User) authentication.getPrincipal();
        model.addAttribute("card", card);
        model.addAttribute("personId", Collections.singletonList(currentUser.getId().toString()));
        return "shop/cardAdd";
    }

    @GetMapping("/allCardsShow")
    public String showFormCardFind(Model model) {
        List<Card> cards = cardService.getAllCards();
        List<CardDto> cardsDto = new ArrayList<>();
        for (Card card :
                cards) {
            CardDto cardDto = new CardDto();
            cardDto.setIdCard(card.getIdCard());
            cardDto.setIdPerson(card.getIdPerson());
            cardDto.setProducts(card.getProducts());
            cardDto.setCost(card.getCost());
            cardsDto.add(cardDto);
        }
        model.addAttribute("cards", cardsDto);
        return "shop/cardsList";
    }

    @PostMapping("/getCost")
    public String submitFormCardAdd(Model model) {
        Long cardId = (Long) model.getAttribute("idCard");
        cardService.calculateCostOfCardById(cardId);
        return "shop/productsCostShow";
    }

    @RequestMapping(value = "/getCost", method = RequestMethod.GET)
    public String showFormCostFind(Model model) {
        List<Long> cardIdList = new ArrayList<>();
        cardRepository.findAll().forEach(el -> cardIdList.add(el.getIdCard()));
        List<Card> cards = cardService.getAllCards();
        List<CardDto> cardsDto = new ArrayList<>();
        for (Card card :
                cards) {
            CardDto cardDto = new CardDto();
            cardDto.setIdCard(card.getIdCard());
            cardDto.setIdPerson(card.getIdPerson());
            cardDto.setProducts(card.getProducts());
            cardDto.setCost(card.getCost());
            cardsDto.add(cardDto);
        }
        model.addAttribute("cards", cardsDto);
        model.addAttribute("cardIdList", cardIdList);
        return "shop/productsCost";
    }


}
