package com.shop.controller;

import com.shop.dtos.ProducDto;
import com.shop.entity.Card;
import com.shop.entity.Product;
import com.shop.entity.User;
import com.shop.service.CardService;
import com.shop.service.ProductService;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CardService cardService;

    @Autowired
    private UserService userService;


    @PostMapping("/productAdd")
    public String submitFormProductAdd(@ModelAttribute("product") ProducDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setPersonId(cardService.getCardById(productDto.getCard_id()).getIdPerson());
        product.setCardId(productDto.getCard_id());
        product.setName(productDto.getName());
        product.setCost(productDto.getCost());
        productService.addProductByPersonId(product);
        cardService.calculateCostOfCardById(product.getCardId());
        return "shop/productAddSucces";
    }

    @GetMapping("/productAdd")
    public String showFormProductAdd(Model model) {
        ProducDto productDto = new ProducDto();
        List<Long> cardIdList = cardService.getAllCards()
                .stream()
                .map(Card::getIdCard)
                .collect(Collectors.toList());

        model.addAttribute("product", productDto);
        model.addAttribute("cardIdList", cardIdList);
        return "shop/productsAdd";
    }

    @GetMapping("/allProductShow")
    public String showFormProductFind(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User) authentication.getPrincipal();
        List<Product> products = productService.getProductsByPersonId(currentUser.getId());
        System.out.println("PRODUCTS " + products);
        List<ProducDto> productDtos = new ArrayList<>();
        ProducDto productDto;
        for (Product pr : products) {
            productDto = new ProducDto();
            productDto.setId(pr.getId());
            productDto.setName(pr.getName());
            productDto.setCost(pr.getCost());
            productDto.setCard_id(pr.getCardId());
            productDtos.add(productDto);
        }
        model.addAttribute("products", productDtos);
        return "shop/productsList";
    }

    @PostMapping("/add")
    public String addProductByPersonId(@RequestBody Product product) {
        return productService.addProductByPersonId(product);
    }

    @GetMapping("/getProductsById")
    public String getProductsById(@RequestParam Long id) {
        return productService.getProductsByPersonIdInString(id);
    }
}
