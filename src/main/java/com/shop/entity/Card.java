package com.shop.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "cards")
@ToString
public class Card {

    @Id
    @Column(name = "idCard")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCard;

    @Column(name = "idPerson")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPerson;
    @Column(name = "cost")
    private BigDecimal cost=null;

    @Column(name = "products")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cardId")
    List<Product> products=new ArrayList<>();
}
