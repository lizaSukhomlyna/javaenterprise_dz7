package com.shop.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@ToString
public class Product {

    @Id
    @Column(name = "productId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "personId")
    private Long personId;

    @Column(name = "cardId")
    private Long cardId;

    private String name;
    private BigDecimal cost;


    public BigDecimal getCost() {
        return cost;
    }

}
