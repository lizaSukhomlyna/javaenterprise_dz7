package com.shop.dtos;

import com.shop.entity.Product;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CardDto {
    private Long idCard;
    private Long idPerson;
    private BigDecimal cost;
    private List<Product> products;
}
