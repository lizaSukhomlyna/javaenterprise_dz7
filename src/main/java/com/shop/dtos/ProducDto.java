package com.shop.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProducDto {

    private Long id;
    private Long personId;
    private Long card_id;
    private String name;
    private BigDecimal cost;

}
