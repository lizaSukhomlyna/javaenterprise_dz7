package com.shop.service;

import com.shop.entity.Card;
import com.shop.entity.Product;
import com.shop.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CardServiceImp implements CardService {

    @Autowired
    private CardRepository cardRepository;

    @Override
    public String createCardByPersonId(Card newCard) {
        try {
            newCard.setCost(BigDecimal.valueOf(0));
            cardRepository.save(newCard);
            return "Card with id:" + newCard.getIdCard() + " was adding for person " + newCard.getIdPerson();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public Card getCardById(Long id) {
        try {
            return cardRepository
                    .findById(id)
                    .orElseThrow(() -> new Exception("This card is missing in database" + new Card()));
        } catch (Exception e) {
            e.printStackTrace();
            return new Card();
        }

    }


    @Override
    public List<Card> getAllCards() {
        List<Card> cards = new ArrayList<>();

        cardRepository.findAll().forEach(
                el -> {
                    el.setCost(calculateCostOfCardById(el.getIdCard()));
                    cards.add(el);
                }
        );
        return cards;
    }

    @Override
    public BigDecimal calculateCostOfCardById(Long cardId) {
        try {
            Card card = cardRepository
                    .findById(cardId)
                    .orElseThrow(() ->
                            new Exception("This card don't find " + new Card()));
            BigDecimal cost = card
                    .getProducts()
                    .stream()
                    .map(Product::getCost)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            card.setCost(cost);
            return cost;
        } catch (Exception e) {
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }
}
