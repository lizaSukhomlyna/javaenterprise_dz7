package com.shop.service;


import com.shop.entity.Card;

import java.math.BigDecimal;
import java.util.List;

public interface CardService {

    String createCardByPersonId(Card card);

    Card getCardById(Long id);

    List<Card> getAllCards();

    BigDecimal calculateCostOfCardById(Long id);


}
