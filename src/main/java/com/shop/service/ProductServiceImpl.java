package com.shop.service;


import com.shop.entity.Product;
import com.shop.repository.ProductRepository;
import com.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public String addProductByPersonId(Product product) {
        try {
            productRepository.save(product);
            return "Product " + product.getName() + " with id:" + product.getId() + " was adding";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public String getProductsByPersonIdInString(Long id) {
        try {
            return productRepository
                    .findById(id)
                    .orElseThrow(() -> new Exception("Product wasn't found" + new Product())).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public List<Product> getProductsByPersonId(Long id) {
        try {
            productRepository.findAll().forEach(System.out::println);
            List<Product> products = new ArrayList<>();
            productRepository.findAll().forEach(el ->
            {
                if (Objects.equals(el.getPersonId(), id)) {
                    products.add(el);
                }
            });
            return products;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
